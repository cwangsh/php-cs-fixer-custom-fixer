<?php

declare(strict_types=1);

namespace Tests;

use Exception;
use InvalidArgumentException;
use PhpCsFixer\Fixer\FixerInterface;
use PhpCsFixer\Linter\Linter;
use PhpCsFixer\Tokenizer\Tokens;
use PHPUnit\Framework\TestCase;
use SplFileInfo;
use Tests\Traits\AssertTokensTrait;

abstract class AbstractFixerTestCase extends TestCase
{
    use AssertTokensTrait;

    protected $fixer;

    final protected function setUp(): void
    {
        $this->fixer = $this->createFixer();
    }

    abstract protected function createFixer(): FixerInterface;

    final public function testFixerDefinitionSummaryStartWithCorrectCase(): void
    {
        self::assertMatchesRegularExpression('/^[A-Z].*\.$/', $this->fixer->getDefinition()->getSummary());
    }

    final public function testFixerDefinitionRiskyDescriptionStartWithLowercase(): void
    {
        if (!$this->fixer->isRisky()) {
            $this->addToAssertionCount(1);

            return;
        }

        $riskyDescription = $this->fixer->getDefinition()->getRiskyDescription();
        assert(is_string($riskyDescription));

        self::assertMatchesRegularExpression('/^[a-z]/', $riskyDescription);
    }

    final public function testFixerDefinitionRiskyDescriptionDoesNotEndWithDot(): void
    {
        if (!$this->fixer->isRisky()) {
            $this->addToAssertionCount(1);

            return;
        }

        $riskyDescription = $this->fixer->getDefinition()->getRiskyDescription();
        assert(is_string($riskyDescription));

        self::assertStringEndsNotWith('.', $riskyDescription);
    }

    final public function testFixerDefinitionHasExactlyOneCodeSample(): void
    {
        self::assertCount(1, $this->fixer->getDefinition()->getCodeSamples());
    }

    final public function testCodeSampleEndsWithNewLine(): void
    {
        $codeSample = $this->fixer->getDefinition()->getCodeSamples()[0];

        self::assertMatchesRegularExpression('/\n$/', $codeSample->getCode());
    }

    final public function testCodeSampleIsChangedDuringFixing(): void
    {
        $codeSample = $this->fixer->getDefinition()->getCodeSamples()[0];

        Tokens::clearCache();
        $tokens = Tokens::fromCode($codeSample->getCode());

        $this->fixer->fix($this->createMock(SplFileInfo::class), $tokens);

        self::assertNotSame($codeSample->getCode(), $tokens->generateCode());
    }

    final public function testPriority(): void
    {
        self::assertIsInt($this->fixer->getPriority());
    }

    final protected function doTest(string $expected, ?string $input = null): void
    {
        if ($expected === $input) {
            throw new InvalidArgumentException('Expected must be different to input.');
        }

        self::assertNull($this->lintSource($expected));

        Tokens::clearCache();
        $expectedTokens = Tokens::fromCode($expected);

        if (null !== $input) {
            Tokens::clearCache();
            $inputTokens = Tokens::fromCode($input);

            self::assertTrue($this->fixer->isCandidate($inputTokens));

            $this->fixer->fix($this->createMock(SplFileInfo::class), $inputTokens);
            $inputTokens->clearEmptyTokens();

            self::assertSame(
                $expected,
                $actual = $inputTokens->generateCode(),
                sprintf(
                    "Expected code:\n```\n%s\n```\nGot:\n```\n%s\n```\n",
                    $expected,
                    $actual
                )
            );

            self::assertTokens($expectedTokens, $inputTokens);
        }

        $this->fixer->fix($this->createMock(SplFileInfo::class), $expectedTokens);

        self::assertSame($expected, $expectedTokens->generateCode());

        self::assertFalse($expectedTokens->isChanged());
    }

    final protected function lintSource(string $source): ?string
    {
        static $linter;

        if (null === $linter) {
            $linter = new Linter;
        }

        try {
            $linter->lintSource($source)->check();
        } catch (Exception $exception) {
            return sprintf('Linting "%s" failed with error: %s.', $source, $exception->getMessage());
        }

        return null;
    }
}
